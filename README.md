See [https://oeis.org/A306385](https://oeis.org/A306385)

`sequence[n]` is maximum number of distinct distances possible between points in a hyperrectangular grid the sum of whose dimensions is n.
