# see https://oeis.org/A306385

import itertools

def partitions(n, k=1):
    if k > n:
        return []
    elif k == n:
        return [[n]]
    else:
        return partitions(n, k+1)+[x+[k] for x in partitions(n-k, k)]

def voxels(box):
    return list(itertools.product(*map(range, box)))

def distances(voxels):
    return set([sum(c*c for c in v) for v in voxels])

def distdist(n):
    return max(len(distances(voxels(p))) for p in partitions(n))

def sequence(n=10):
    for i in range(1, n+1):
        print('{:2d} -> {:d}'.format(i, distdist(i)))

