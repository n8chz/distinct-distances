#!/usr/bin/ruby

def partitions(n, k = 1)
  if k > n
    []
  elsif k == n
    [[n]]
  else
    partitions(n, k+1)+partitions(n-k, k).map {|p| p << k}
  end
end

def voxels(box)
  first = box.shift
  value = (0...first).map {|x| [x]}
  box.each do |dim|
    value = value.product((0...dim).to_a).map {|x, y| x+[y]}
  end
  value
end

def distances(voxels)
  voxels.map do |v|
    v.map do |c|
      c*c
    end.sum
  end.uniq
end

def sequence(top = 100)
  (1..top).map do |n|
    # p partitions(n)
    partitions(n).map do |p|
      [n, distances(voxels(p)).count, p]
    end.max_by {|n, d, p| d}
  end.map {|n, d, p| d}
end

p sequence(25).join(",")

